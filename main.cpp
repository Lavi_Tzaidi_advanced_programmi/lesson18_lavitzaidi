#include "Helper.h"
#include <Windows.h>

std::string pwd();
void cd(std::string command);
void create(std::string command);
void ls();
void secret();
int main()
{
	int flag = 1;
	std::string command = "";
	
	while (1)
	{
		std::cout << "<<";
		std::getline(std::cin, command);
		Helper::trim(command);  //Removes all the white spaces from the command
		/*
		this part is for the PWD function 
		*/
		if (command.compare("pwd") == 0)
		{
			pwd();
		}
		/*
		this part is to create the function CD 
		*/
		std::string checkCD = command;
		checkCD = checkCD.substr(0,2);
		if (checkCD.compare("cd") == 0)
		{
			cd(command);
		}
		/*
		this part is to cheack option 4 of the create option
		*/
		std::string checkCreate = command;
		Helper::trim(checkCreate);
		checkCreate = checkCreate.substr(0, 6);
		if (checkCreate.compare("create") == 0)
		{
			create(command);
		}
		/*
			this part is for the ls function ///// ls ls ls ls ls ls ls ls ls 
		*/
		std::string checkLS = command;
		Helper::trim(checkLS);
		checkLS = checkLS.substr(0, 2);
		if (checkLS.compare("ls") == 0)
		{
			ls();
		}
		/*
		this part is for the secret function           secrettttttttttttttttttttt
		*/
		std::string checkSec = command;
		Helper::trim(checkSec);
		checkSec = checkSec.substr(0, 6);
		if (checkSec.compare("secret") == 0)
		{
			secret();
		}

		/*
		this is for the last function 7 //////////////////////
		*/


		
	}
	

	system("PAUSE");
	return 0;
}
std::string pwd()
{
	int flag = 0;
	char buffer[MAX_PATH];
	flag = GetCurrentDirectory(MAX_PATH, buffer);
	if (flag != 0)  //Checks if everything is OK
	{
		std::cout << buffer << std::endl;
	}
	else
	{
		std::cout << GetLastError() << std::endl;  //Prints the error
	}
	return buffer;
}
void cd(std::string command)
{
	std::string directory = command;
	directory = directory.substr(2);
	Helper::trim(directory);

	int flag = SetCurrentDirectory(directory.c_str());
	if (flag == 0)  //Checks if everything is OK
	{
		std::cout << GetLastError() << std::endl;  //Prints the error
	}
	std::cout << command << std::endl;
}
void ls()
{
	HANDLE hFile;
	WIN32_FIND_DATA fdFile;
	hFile = FindFirstFile((pwd() + "\\*.*").c_str(), &fdFile);
	if (hFile != INVALID_HANDLE_VALUE)
	{
		do
		{
			printf("%s \n", fdFile.cFileName);
		} while (FindNextFile(hFile, &fdFile));
	}
	FindClose(hFile);
}
void create(std::string command)
{
	std::string checkCreate = command;
	checkCreate = checkCreate.substr(6);
	Helper::trim(checkCreate);
	HANDLE hFile;
	hFile = CreateFile(checkCreate.c_str(), GENERIC_WRITE, 3, NULL, 2, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile == INVALID_HANDLE_VALUE)
	{
		std::cout << GetLastError() << std::endl;  //Prints the error
	}

}
void secret()
{
	typedef int(__stdcall *f_funci)();
	HINSTANCE hGetProcIDDLL = LoadLibrary("Secret.dll");

	if (!hGetProcIDDLL)
	{
		std::cout << "could not load the dynamic library" << std::endl;
		return;
	}

	// resolve function address here
	f_funci funci = (f_funci)GetProcAddress(hGetProcIDDLL, "TheAnswerToLifeTheUniverseAndEverything");
	if (!funci)
	{
		std::cout << "could not locate the function" << std::endl;
		return;
	}

	std::cout << funci() << std::endl;
}
